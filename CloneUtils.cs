﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    public static class CloneUtils
    {
        public static TValue Clone<TValue>(TValue orig) where TValue : IClonable<TValue>
        {
            if (orig == null)
                return default(TValue);
            return orig.MakeClone();
        }

        public static TValue Clone<TValue,TValueBase>(TValue orig) where TValue : class, IClonable<TValueBase>, TValueBase
        {
            if (orig == null)
                return default(TValue);
            return orig.MakeClone() as TValue;
        }

        public static List<TValue> CloneList<TValue>(List<TValue> orig) where TValue : IClonable<TValue>
        {
            if (orig == null)
                return null;

            var clone = new List<TValue>();
            foreach (var item in orig)
                clone.Add(item != null ? item.MakeClone() : default(TValue));

            return clone;
        }


        public static List<TValue> CloneList<TValue,TValueBase>(List<TValue> orig) where TValue : class, IClonable<TValueBase>, TValueBase
        {
            if (orig == null)
                return null;

            var clone = new List<TValue>();
            foreach (var item in orig)
                clone.Add(Clone<TValue, TValueBase>(item));

            return clone;
        }

        public static List<TValue> CloneListSimple<TValue>(IReadOnlyList<TValue> orig)
        {
            if (orig == null)
                return null;

            var clone = new List<TValue>(orig);

            return clone;
        }

        public static TValue[] CloneArray<TValue>(TValue[] orig) where TValue : IClonable<TValue>
        {
            if (orig == null)
                return null;

            var clone = new TValue[orig.Length];
            for (var i = 0; i < orig.Length; i++)
                clone[i] = Clone(orig[i]);

            return clone;
        }


        public static TValue[] CloneArray<TValue,TValueBase>(TValue[] orig) where TValue : class, IClonable<TValueBase>, TValueBase
        {
            if (orig == null)
                return null;

            var clone = new TValue[orig.Length];
            for (var i = 0; i < orig.Length; i++)
                clone[i] = Clone<TValue, TValueBase>(orig[i]);

            return clone;
        }

        public static TValue[] CloneArraySimple<TValue>(TValue[] orig)
        {
            if (orig == null)
                return null;

            var clone = new TValue[orig.Length];
            Array.Copy(orig, clone, orig.Length);

            return clone;
        }

        public static Dictionary<TKey, TValue> CloneDictionary<TKey, TValue>(Dictionary<TKey, TValue> orig) where TValue : IClonable<TValue>
        {
            if (orig == null)
                return null;

            var clone = new Dictionary<TKey, TValue>();
            foreach (var kp in orig)
                clone.Add(kp.Key, Clone(kp.Value));

            return clone;
        }

        public static Dictionary<TKey, TValue> CloneDictionary<TKey, TValue, TValueBase>(Dictionary<TKey, TValue> orig) where TValue : class, IClonable<TValueBase>, TValueBase
        {
            if (orig == null)
                return null;

            var clone = new Dictionary<TKey, TValue>();
            foreach (var kp in orig)
                clone.Add(kp.Key, Clone<TValue, TValueBase>(kp.Value));

            return clone;
        }

        public static Dictionary<TKey, TValue> CloneDictionarySimple<TKey, TValue>(Dictionary<TKey, TValue> orig)
        {
            if (orig == null)
                return null;

            var clone = new Dictionary<TKey, TValue>(orig);

            return clone;
        }

        public static Dictionary<TKey, List<TValue>> CloneDictionaryListSimple<TKey, TValue>(Dictionary<TKey, List<TValue>> orig)
        {
            if (orig == null)
                return null;

            var clone = new Dictionary<TKey, List<TValue>>();
            foreach (var kp in orig)
            {
                var list = kp.Value != null ? new List<TValue>(kp.Value) : null;
                clone.Add(kp.Key, list);
            }

            return clone;
        }
    }
}
