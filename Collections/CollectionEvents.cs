﻿namespace Pump.Core
{
    public enum CollectionEventType : int
    {
        _First = 20100,

        ItemAdded,
        ItemRemoved,
        ItemChanged, // not used for now?
        ValueChanged,
        Cleared // not used for now?
    }

    public class ItemCollectionEvent<TKey> : IEvent
    {
        public int eventType { get; private set; }

        public TKey itemId { get; private set; }

        public ItemCollectionEvent(int eventType, TKey itemId)
        {
            this.eventType = eventType;
            this.itemId = itemId;
        }
    }

    public class ValueChangedEvent<TValue> : IEvent
    {
        public int eventType { get { return CollectionEventType.ValueChanged.ToInt(); } }
        public string itemId { get; private set; }
        public TValue itemValue { get; private set; }
        public TValue oldItemValue { get; private set; }

        public ValueChangedEvent(string itemId, TValue itemValue, TValue oldItemValue)
        {
            this.itemId = itemId;
            this.itemValue = itemValue;
            this.oldItemValue = oldItemValue;
        }
    }
}
