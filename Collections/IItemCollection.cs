﻿using System;

namespace Pump.Core
{
    public interface IItemCollection : IHaveId<string>, IHaveEvents, IDisposable
    {
        bool IsEmpty();
        int GetItemCount();
    }
}
