﻿using System.Collections.Generic;
using System.Linq;

namespace Pump.Core
{
    public class IntValueCollection : IItemCollection
    {
        public string id { get; set; }

        private Dictionary<string, int> items;

        public IReadOnlyDictionary<string, int> itemsRO => items;

        public EventMessenger events { get; private set; }

        public IntValueCollection(string id = null)
        {
            this.id = id;
            events = new EventMessenger();
            items = new Dictionary<string, int>();
        }

        public bool IsEmpty()
        {
            return items.Count == 0;
        }

        public int GetItemCount()
        {
            return items.Count;
        }

        public int GetValue(string key)
        {
            if (string.IsNullOrEmpty(key) || !items.ContainsKey(key))
                return 0;
            return items[key];
        }

        public Dictionary<string, int> GetAllValues(bool nullIfEmpty = false)
        {
            if (nullIfEmpty && IsEmpty())
                return null;
            return new Dictionary<string, int>(items);
        }

        public void SetValues(Dictionary<string, int> values, bool raiseEvent = true)
        {
            var removeKeys = values == null ? items.Keys.ToList() : items.Keys.Where(key => !values.ContainsKey(key) || values[key] == 0).ToList();
            foreach (var key in removeKeys)
                SetValue(key, 0);

            if (values == null)
                return;

            foreach (var keyPair in values)
                SetValue(keyPair.Key, keyPair.Value, raiseEvent);
        }

        public void SetValue(string key, int value, bool raiseEvent = true)
        {
            int oldValue = 0;
            items.TryGetValue(key, out oldValue);

            if (value == 0)
            {
                if (items.ContainsKey(key))
                {
                    items.Remove(key);
                    if (raiseEvent)
                        events.Raise(new ValueChangedEvent<int>(key, 0, oldValue));
                }
                return;
            }

            if (items.ContainsKey(key))
                items[key] = value;
            else
                items.Add(key, value);

            if (raiseEvent)
                events.Raise(new ValueChangedEvent<int>(key, value, oldValue));
        }

        public void AddValue(string key, int addValue, bool raiseEvent = true)
        {
            SetValue(key, GetValue(key) + addValue, raiseEvent);
        }

        public void Clear(bool raiseEvent = true)
        {
            if (IsEmpty()) return;

            if (!raiseEvent)
            {
                items.Clear();
                return;
            }

            SetValues(null, raiseEvent);
            events.Raise(new SimpleEvent(CollectionEventType.Cleared.ToInt()));
        }

        public void Dispose()
        {
            Clear(false);
            events = events.SafeDispose();
        }
    }
}
