﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pump.Core
{
    public class ItemCollection<TKey, TItem> : IItemCollection where TItem : IHaveId<TKey>
    {
        public string id { get; set; }

        protected Dictionary<TKey, TItem> items = new Dictionary<TKey, TItem>();

        public EventMessenger events { get; private set; }

        public bool isDisposed { get; private set; }

        public ItemCollection(string id = null)
        {
            this.id = id;
            events = new EventMessenger();
        }

        public bool IsEmpty()
        {
            return items.Count == 0;
        }

        public int GetItemCount()
        {
            return items.Count;
        }

        public void AddItem(TItem item, bool fireEvent = true)
        {
            if (item == null || items.ContainsKey(item.id))
                throw new Exception();

            items.Add(item.id, item);

            if (fireEvent)
                events.Raise(new ItemCollectionEvent<TKey>(CollectionEventType.ItemAdded.ToInt(), item.id));
        }

        public void RemoveItem(TKey key, bool keepAlive = false)
        {
            if (key == null || !items.ContainsKey(key))
                throw new Exception();

            if (!keepAlive)
            {
                var item = items[key];
                var disposableItem = item as IDisposable;
                if (disposableItem != null)
                    disposableItem.Dispose();
            }

            items.Remove(key);

            events.Raise(new ItemCollectionEvent<TKey>(CollectionEventType.ItemRemoved.ToInt(), key));
        }

        public TItem GetItem(TKey key)
        {
            if (key == null || !items.ContainsKey(key))
                return default(TItem);
            return items[key];
        }

        public bool ContainsItem(TKey key)
        {
            if (key == null || !items.ContainsKey(key))
                return false;
            return true;
        }

        public IEnumerable<TKey> GetAllKeys()
        {
            return items.Keys;
        }

        public IEnumerable<TItem> GetAllItems()
        {
            return items.Values;
        }

        public void Clear(bool keepAlive = false)
        {
            if (!keepAlive)
            {
                foreach (var key in items.Keys)
                {
                    var item = items[key];
                    var disposableItem = item as IDisposable;
                    if (disposableItem != null)
                        disposableItem.Dispose();
                    else
                        break; // не тратить время на остальных
                }
            }

            items.Clear();
            events.Raise(new SimpleEvent(CollectionEventType.Cleared.ToInt()));
        }

        public void Dispose()
        {
            if (isDisposed) return;

            isDisposed = true;

            Clear();

            events.Dispose();
            events = null;
        }
    }
}
