﻿namespace Pump.Core
{
    // collection of collections
    public class Repository : ItemCollection<string,IItemCollection>
    {
        public ItemCollection<TKey,TItem> GetColllection<TKey,TItem>(string id) where TItem : IHaveId<TKey>
        {
            if (string.IsNullOrEmpty(id)) return null;

            return GetItem(id) as ItemCollection<TKey, TItem>;
        }
    }
}
