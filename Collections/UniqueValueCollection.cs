﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pump.Core
{
    public class UniqueValueCollection<TValue> : IItemCollection
    {
        public string id { get; set; }

        protected HashSet<TValue> items = new HashSet<TValue>();

        public EventMessenger events { get; private set; }

        public bool isDisposed { get; private set; }

        // need to refactor?
        public bool exceptionOnDefaultValue = true;
        public bool exceptionOnDublicate = false;

        public UniqueValueCollection(string id = null)
        {
            this.id = id;
            events = new EventMessenger();
        }

        public bool IsEmpty()
        {
            return items.Count == 0;
        }

        public int GetItemCount()
        {
            return items.Count;
        }

        public void SetValues(List<TValue> values, bool raiseEvent = true)
        {
            var removeKeys = values == null ? items.ToList() : items.Where(key => !values.Contains(key)).ToList();
            foreach (var key in removeKeys)
                RemoveItem(key, raiseEvent);

            if (values == null)
                return;

            var addKeys = values.Where(key => !items.Contains(key));
            foreach (var val in addKeys)
                AddItem(val, raiseEvent);
        }

        public void AddItem(TValue item, bool raiseEvent = true)
        {
            if (exceptionOnDefaultValue && item.Equals(default(TValue)))
                throw new Exception();

            var success = items.Add(item);

            if (!success)
            {
                if (exceptionOnDublicate)
                    throw new Exception();
                return;
            }

            if (raiseEvent)
                events.Raise(new ItemCollectionEvent<TValue>((int)CollectionEventType.ItemAdded, item));
        }

        public void RemoveItem(TValue item, bool raiseEvent = true)
        {
            if (exceptionOnDefaultValue && item.Equals(default(TValue)))
                throw new Exception();

            var success = items.Remove(item);

            if (!success)
                return;

            if (raiseEvent)
                events.Raise(new ItemCollectionEvent<TValue>((int)CollectionEventType.ItemRemoved, item));
        }

        public bool ContainsItem(TValue item)
        {
            if (item.Equals(default(TValue)))
                return false;

            return items.Contains(item);
        }

        public List<TValue> GetAllItems(bool nullIfEmpty = false)
        {
            if (nullIfEmpty && IsEmpty())
                return null;
            var keys = new List<TValue>(items);
            return keys;
        }

        public void Clear(bool raiseEvent = true)
        {
            if (IsEmpty()) return;

            if (!raiseEvent)
            {
                items.Clear();
                return;
            }

            SetValues(null, raiseEvent);
            events.Raise(new SimpleEvent(CollectionEventType.Cleared.ToInt()));
        }

        public void Dispose()
        {
            if (isDisposed) return;

            isDisposed = true;

            Clear();

            events.Dispose();
            events = null;
        }
    }
}
