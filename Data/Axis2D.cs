﻿namespace Pump.Core
{
    public enum Axis2D
    {
        None,
        Horizontal,
        Vertical,
        Both
    }
}
