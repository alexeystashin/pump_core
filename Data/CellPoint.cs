﻿using System;

namespace Pump.Core
{
    [Serializable]
    [ProtoBuf.ProtoContract]
    public struct CellPoint : IEquatable<CellPoint>
    {
        [ProtoBuf.ProtoMember(1)]
        public int x;

        [ProtoBuf.ProtoMember(2)]
        public int z;

        public bool IsValid() => _isValid;

        [ProtoBuf.ProtoMember(3)]
        private bool _isValid;

        public CellPoint(int x, int z)
        {
            this.x = x;
            this.z = z;
            _isValid = true;
        }

        public override string ToString()
        {
            return "[" + x + "," + z + "]";
        }

        public static CellPoint operator +(CellPoint left, CellPoint right)
        {
            return new CellPoint(left.x + right.x, left.z + right.z);
        }

        public static CellPoint operator -(CellPoint left, CellPoint right)
        {
            return new CellPoint(left.x - right.x, left.z - right.z);
        }

        public static bool operator ==(CellPoint left, CellPoint right)
        {
            return left.x == right.x && left.z == right.z && left._isValid == right._isValid;
        }

        public static bool operator !=(CellPoint left, CellPoint right)
        {
            return !(left == right);
        }

        public bool Equals(CellPoint right)
        {
            return x == right.x && z == right.z && _isValid == right._isValid;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is CellPoint)) return false;
            var right = (CellPoint)obj;
            return x == right.x && z == right.z && _isValid == right._isValid;
        }

        public override int GetHashCode()
        {
            return x ^ z;
        }
    }
}
