﻿using System;

namespace Pump.Core
{
    [Serializable]
    [ProtoBuf.ProtoContract]
    public struct CellRect : IEquatable<CellRect>
    {
        [ProtoBuf.ProtoMember(1)]
        public CellPoint pos;

        [ProtoBuf.ProtoMember(2)]
        public CellPoint size;

        public CellRect(CellPoint aPos, CellPoint aSize)
        {
            pos = aPos;
            size = aSize;
        }

        public bool IsValid()
        {
            return pos.IsValid() && size.IsValid();
        }

        public bool IsInRect(CellPoint point)
        {
            return (point.x >= pos.x && point.x < pos.x + size.x && point.z >= pos.z && point.z < pos.z + size.z);
        }

        public bool Equals(CellRect other)
        {
            return pos.Equals(other.pos) && size.Equals(other.size);
        }

        public override string ToString()
        {
            return string.Format("[CellRect pos={0} size={1}]", pos, size);
        }
    }
}
