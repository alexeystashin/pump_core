﻿namespace Pump.Core
{
    [ProtoBuf.ProtoContract]
    public class ColorData : IClonable<ColorData>
    {
        [ProtoBuf.ProtoMember(1)]
        public byte r;

        [ProtoBuf.ProtoMember(2)]
        public byte g;

        [ProtoBuf.ProtoMember(3)]
        public byte b;

        [ProtoBuf.ProtoMember(4)]
        public byte a;

        public ColorData() { }

        public ColorData(byte r, byte g, byte b, byte a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public ColorData MakeClone()
        {
            var clone = new ColorData
            {
                r = r,
                g = g,
                b = b,
                a = a
            };
            return clone;
        }
    }
}
