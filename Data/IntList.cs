﻿using System.Collections.Generic;

namespace Pump.Core
{
    [ProtoBuf.ProtoContract]
    public class IntList : IClonable<IntList>
    {
        [ProtoBuf.ProtoMember(1)]
        public List<int> items;

        public IntList MakeClone()
        {
            var clone = new IntList
            {
                items = CloneUtils.CloneListSimple(items)
            };

            return clone;
        }
    }
}
