﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pump.Core
{
    [ProtoBuf.ProtoContract]
    public class LerpAmount : IClonable<LerpAmount>
    {
        [ProtoBuf.ProtoMember(1)]
        public List<float> values;

        #region constructors

        public LerpAmount(List<float> values)
        {
            this.values = new List<float>(values);
        }

        public LerpAmount(float a, float b)
        {
            values = new List<float> {a, b};
        }

        public LerpAmount(float amount)
        {
            values = new List<float> {amount};
        }

        public LerpAmount()
        {
        }
        #endregion

        public float GetAmount(float t) // t = [0..1]
        {
            if (values == null || values.Count == 0)
                return 0;
            if (values.Count == 1)
                return values[0];

            t = Math.Max(0, Math.Min(1, t));

            var tStep = 1.0f / (values.Count-1);
            var valIdx = (int)(t / tStep) + 1;
            valIdx = Math.Min(Math.Max(valIdx, 0), values.Count - 1); // clamp
            t = t % tStep;
            var a = values[Math.Max(0, valIdx - 1)];
            var b = values[valIdx];

            return a + (b - a) * t;
        }

        public float GetAverageAmount()
        {
            if (values == null || values.Count == 0)
                return 0;
            if (values.Count == 1)
                return values[0];
            return values.Sum() / values.Count;
        }

        public LerpAmount MakeClone()
        {
            var clone = new LerpAmount
            {
                values = CloneUtils.CloneListSimple(values)
            };

            return clone;
        }
    }
}
