﻿using System;

namespace Pump.Core
{
    // add derived classes via RuntimeTypeModel.Default[typeof(BaseClass)].AddSubType(CLASS_INDEX, typeof(DerivedClass));

    [ProtoBuf.ProtoContract]
    public partial class PumpEvent : IEvent, IClonable<PumpEvent>
    {
        [ProtoBuf.ProtoMember(1)]
        public int eventType { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public string roomId;

        [ProtoBuf.ProtoMember(3)]
        public long ts;

        public PumpEvent()
        {
            ts = DateTime.Now.Ticks;
        }

        public virtual PumpEvent MakeClone()
        {
            var clone = new PumpEvent();
            CopyPropertiesTo(clone);
            return clone;
        }

        protected virtual void CopyPropertiesTo(PumpEvent target)
        {
            var clone = target;
            clone.eventType = eventType;
            clone.roomId = roomId;
            clone.ts = ts;
        }

        public override string ToString()
        {
            return string.Format("[{0} eventType={1}, roomId={2}]", GetType().Name, eventType, roomId);
        }
    }
}
