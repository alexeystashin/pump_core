﻿using System;

namespace Pump.Core
{
    // add derived classes via RuntimeTypeModel.Default[typeof(BaseClass)].AddSubType(CLASS_INDEX, typeof(DerivedClass));

    [ProtoBuf.ProtoContract]
    public partial class PumpRequest : IClonable<PumpRequest>
    {
        [ProtoBuf.ProtoMember(1)]
        public byte opCode;

        [ProtoBuf.ProtoMember(2)]
        public string roomId;

        [ProtoBuf.ProtoMember(3)]
        public int requestId;

        [ProtoBuf.ProtoMember(4)]
        public long ts;

        public PumpRequest()
        {
            ts = DateTime.Now.Ticks;
        }

        public virtual PumpRequest MakeClone()
        {
            var clone = new PumpRequest();
            CopyPropertiesTo(clone);
            return clone;
        }

        protected virtual void CopyPropertiesTo(PumpRequest target)
        {
            var clone = target;
            clone.opCode = opCode;
            clone.roomId = roomId;
            clone.requestId = requestId;
            clone.ts = ts;
        }

        public override string ToString()
        {
            return string.Format("[{0} opCode={1}, roomId={2}, requestId={3}]", GetType().Name, opCode, roomId, requestId);
        }
    }
}
