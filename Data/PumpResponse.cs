﻿using System;

namespace Pump.Core
{
    // add derived classes via RuntimeTypeModel.Default[typeof(BaseClass)].AddSubType(CLASS_INDEX, typeof(DerivedClass));

    [ProtoBuf.ProtoContract]
    public partial class PumpResponse : IClonable<PumpResponse>
    {
        [ProtoBuf.ProtoMember(1)]
        public byte opCode;

        [ProtoBuf.ProtoMember(2)]
        public string roomId;

        [ProtoBuf.ProtoMember(3)]
        public int requestId;

        [ProtoBuf.ProtoMember(4)]
        public long ts;

        [ProtoBuf.ProtoMember(5)]
        public short returnCode;

        [ProtoBuf.ProtoMember(6)]
        public string debugMessage;

        public PumpResponse()
        {
            ts = DateTime.Now.Ticks;
        }

        public virtual PumpResponse MakeClone()
        {
            var clone = new PumpResponse();
            CopyPropertiesTo(clone);
            return clone;
        }

        protected virtual void CopyPropertiesTo(PumpResponse target)
        {
            var clone = target;

            clone.opCode = opCode;
            clone.roomId = roomId;
            clone.requestId = requestId;
            clone.ts = ts;
            clone.returnCode = returnCode;
            clone.debugMessage = debugMessage;
        }

        public override string ToString()
        {
            return string.Format("[{0} opCode={1}, roomId={2}, requestId={3}, returnCode={4}, debugMessage={5}]",
                GetType().Name, opCode, roomId, requestId, returnCode, debugMessage);
        }
    }
}
