﻿using System;

namespace Pump.Core
{
    [Serializable]
    [ProtoBuf.ProtoContract]
    public struct RealPoint : IEquatable<RealPoint>
    {
        public const float E = 0.001f;

        [ProtoBuf.ProtoMember(1)]
        public float x;

        [ProtoBuf.ProtoMember(2)]
        public float z;

        public bool IsValid() => _isValid;

        [ProtoBuf.ProtoMember(3)]
        private bool _isValid;

        public RealPoint(float x, float z)
        {
            this.x = x;
            this.z = z;
            _isValid = true;
        }

        public override string ToString()
        {
            return "[" + x + "," + z + "]";
        }

        public static RealPoint operator +(RealPoint left, RealPoint right)
        {
            return new RealPoint(left.x + right.x, left.z + right.z);
        }

        public static RealPoint operator -(RealPoint left, RealPoint right)
        {
            return new RealPoint(left.x - right.x, left.z - right.z);
        }

        public static bool operator ==(RealPoint left, RealPoint right)
        {
            return Math.Abs(left.x - right.x) < E && Math.Sign(left.z - right.z) < E && left._isValid == right._isValid;
        }

        public static bool operator !=(RealPoint left, RealPoint right)
        {
            return !(left == right);
        }

        public bool Equals(RealPoint right)
        {
            return Math.Abs(x - right.x) < E && Math.Sign(z - right.z) < E && _isValid == right._isValid;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is RealPoint)) return false;
            var right = (RealPoint)obj;
            return Math.Abs(x - right.x) < E && Math.Sign(z - right.z) < E && _isValid == right._isValid;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + x.GetHashCode();
            hash = hash * 23 + z.GetHashCode();
            return hash;
        }

        public static RealPoint GetRandomPointInside(CellPoint cellPoint)
        {
            return new RealPoint(
                cellPoint.x + RandomUtils.GetInRange(RealPoint.E, 1.0f - RealPoint.E),
                cellPoint.z + RandomUtils.GetInRange(RealPoint.E, 1.0f - RealPoint.E)
                );
        }

        public static RealPoint GetCenterPoint(CellPoint cellPoint)
        {
            return new RealPoint(cellPoint.x + 0.5f, cellPoint.z + 0.5f);
        }

        public CellPoint ToCellPoint()
        {
            if (!_isValid)
                return default;
            return new CellPoint((int)x, (int)z);
        }
    }
}
