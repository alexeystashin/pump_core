﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    [Serializable]
    [ProtoBuf.ProtoContract]
    public struct TransformPoint
    {
        public const float E = 0.001f;

        [ProtoBuf.ProtoMember(1)]
        public float x;

        [ProtoBuf.ProtoMember(2)]
        public float z;

        [ProtoBuf.ProtoMember(3)]
        public float rot; // degrees, counter-clockwise

        public bool IsValid() => _isValid;

        [ProtoBuf.ProtoMember(3)]
        private bool _isValid;

        public TransformPoint(float x, float z, float rot)
        {
            this.x = x;
            this.z = z;
            this.rot = rot;
            _isValid = true;
        }

        public TransformPoint(RealPoint point, float rot)
        {
            this.x = point.x;
            this.z = point.z;
            this.rot = rot;
            _isValid = true;
        }

        public override string ToString()
        {
            return $"[{x},{z},{rot}]";
        }

        public static TransformPoint Transform(TransformPoint parentPoint, TransformPoint localPoint)
        {
            var angle = parentPoint.rot * Math.PI / 180.0f;
            var s = Math.Sin(angle);
            var c = Math.Cos(angle);

            // rotate local point
            var lX = (float)(localPoint.x * c - localPoint.z * s);
            var lZ = (float)(localPoint.x * s + localPoint.z * c);

            // add parent point:
            var resultPoint = new TransformPoint(
                lX + parentPoint.x,
                lZ + parentPoint.z,
                localPoint.rot + parentPoint.rot);
            return resultPoint;
        }

        public static TransformPoint TransformToLocal(TransformPoint parentPoint, TransformPoint point)
        {
            var localPoint = new TransformPoint(point.ToRealPoint() - parentPoint.ToRealPoint(), point.rot);
            var resultPoint = Transform(new TransformPoint(0, 0, -parentPoint.rot), localPoint);
            return resultPoint;
        }

        public static void Transform(TransformPoint parentPoint, List<TransformPoint> localPoints)
        {
            for(var i = 0; i < localPoints.Count; i++)
                localPoints[i] = Transform(parentPoint, localPoints[i]);
        }

        public CellPoint ToCellPoint()
        {
            return new CellPoint((int)x, (int)z);
        }

        public RealPoint ToRealPoint()
        {
            return new RealPoint(x, z);
        }
    }
}
