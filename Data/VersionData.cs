﻿using System;

namespace Pump.Core
{
    [ProtoBuf.ProtoContract]
    public class VersionData : IComparable, IClonable<VersionData>
    {
        [ProtoBuf.ProtoMember(1)]
        public int major;

        [ProtoBuf.ProtoMember(2)]
        public int minor;

        [ProtoBuf.ProtoMember(3)]
        public int build;

        [ProtoBuf.ProtoMember(4)]
        public int revision;

        public VersionData()
        {
        }

        public VersionData(int major, int minor, int build, int revision = 0)
        {
            this.major = major;
            this.minor = minor;
            this.build = build;
            this.revision = revision;
        }

        public VersionData(string versionStr)
        {
            var ver = new Version(versionStr);
            major = ver.Major;
            minor = ver.Minor;
            build = ver.Build;
            revision = ver.Revision;
        }

        public VersionData MakeClone()
        {
            return new VersionData()
            {
                major = major,
                minor = minor,
                build = build,
                revision = revision
            };
        }

        public int CompareTo(object version)
        {
            if (version == null)
                return 1;

            if (!(version is VersionData))
                throw new ArgumentException("Arg_MustBeVersion");

            var otherVersion = (VersionData) version;
            if (major != otherVersion.major)
            {
                if (major > otherVersion.major)
                    return 1;
                return -1;
            }
            if (minor != otherVersion.minor)
            {
                if (minor > otherVersion.minor)
                    return 1;
                return -1;
            }
            if (build != otherVersion.build)
            {
                if (build > otherVersion.build)
                    return 1;
                return -1;
            }
            return
                revision - otherVersion.revision;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is VersionData))
                return false;
            var otherVersion = (VersionData) obj;
            return (major == otherVersion.major) && (minor == otherVersion.minor) && (build == otherVersion.build) &&
                   (revision == otherVersion.revision);
        }

        public override int GetHashCode()
        {
            int num1 = 0;
            num1 |= ((major & 15) << 0x1c);
            num1 |= ((minor & 0xff) << 20);
            num1 |= ((build & 0xff) << 12);
            return (num1 | revision & 0xfff);
        }

        public static bool operator ==(VersionData v1, VersionData v2)
        {
            if ((object) v1 == null)
                return (object) v2 == null;
            return v1.Equals(v2);
        }

        public static bool operator >(VersionData v1, VersionData v2)
        {
            return (v2 < v1);
        }

        public static bool operator >=(VersionData v1, VersionData v2)
        {
            return (v2 <= v1);
        }

        public static bool operator !=(VersionData v1, VersionData v2)
        {
            return !(v1 == v2);
        }

        public static bool operator <(VersionData v1, VersionData v2)
        {
            if (v1 == null)
            {
                throw new ArgumentNullException("v1");
            }
            return (v1.CompareTo(v2) < 0);
        }

        public static bool operator <=(VersionData v1, VersionData v2)
        {
            if (v1 == null)
            {
                throw new ArgumentNullException("v1");
            }
            return (v1.CompareTo(v2) <= 0);
        }

        public override string ToString()
        {
            return major + "." + minor + "." + build + (revision > 0 ? "." + revision : "");
        }
    }
}