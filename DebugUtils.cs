﻿using System;
using System.Linq;

namespace Pump.Core
{
    public enum DebugType
    {
        Default,
        Net,
        NetFull
    };

    public static class DebugUtils
    {
        public static DebugType[] debugLogFilter = new DebugType[]
        {
            DebugType.Default,
            DebugType.Net
        };

#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_IPHONE || UNITY_STANDALONE || UNITY_ANDROID || UNITY_WEBGL
        public static void Log(string message, DebugType type=DebugType.Default)
        {
            if (debugLogFilter.Contains(type))
                UnityEngine.Debug.Log(message);
        }

        public static void LogWarning(string message, DebugType type = DebugType.Default)
        {
            if (debugLogFilter.Contains(type))
                UnityEngine.Debug.LogWarning(message);
        }

        public static void LogError(string message)
        {
            UnityEngine.Debug.LogError(message);
        }
#else
        public static Action<string, DebugType> OnLog;
        public static Action<string, DebugType> OnLogWarning;
        public static Action<string> OnLogError;

        public static void Log(string message, DebugType type = DebugType.Default)
        {
            if (OnLog != null && debugLogFilter.Contains(type)) OnLog(message, type);
        }

        public static void LogWarning(string message, DebugType type = DebugType.Default)
        {
            if (OnLogWarning != null && debugLogFilter.Contains(type)) OnLogWarning(message, type);
        }

        public static void LogError(string message)
        {
            if (OnLogError != null) OnLogError(message);
        }
#endif

    }
}
