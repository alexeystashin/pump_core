﻿using System;

namespace Pump.Core
{
    public static partial class Utils
    {
        // Usage example:
        // Utils.SafeDispose(ref objToDispose);
        public static void SafeDispose<T>(ref T obj) where T : class, IDisposable
        {
            if (!ReferenceEquals(obj, null))
                obj.Dispose();
            obj = null;
        }

        // Usage example:
        // objToDispose = objToDispose.SafeDispose();
        public static T SafeDispose<T>(this T obj) where T : class, IDisposable
        {
            if (!ReferenceEquals(obj, null))
                obj.Dispose();
            return null;
        }
    }
}
