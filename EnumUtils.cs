﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pump.Core
{
    public static class EnumUtils
    {
        public static IEnumerable<TEnum> GetAllValues<TEnum>()
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
        }

        public static TEnum ParseValue<TEnum>(string valueStr, TEnum defaultValue)
            // where TEnum : struct, IConvertible
        {
            TEnum val;
            return TryParseValue(valueStr, out val) ? val : defaultValue;
        }

        public static bool TryParseValue<TEnum>(string value, out TEnum result)
            // where TEnum : struct, IConvertible
        {
            var retValue = value != null && Enum.IsDefined(typeof(TEnum), value);
            result = retValue
                ? (TEnum)Enum.Parse(typeof(TEnum), value)
                : default(TEnum);
            return retValue;
        }

        public static int ToInt(this Enum enumVal)
        {
            return Convert.ToInt32(enumVal);
        }

        public static byte ToByte(this Enum enumVal)
        {
            return Convert.ToByte(enumVal);
        }
    }
}
