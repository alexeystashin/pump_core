﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    public class EventMessenger : IDisposable
    {
        public delegate void EventDelegate(IEvent e);

        readonly Dictionary<int, Delegate> delegates = new Dictionary<int, Delegate>();

        Delegate anyEventDelegate;

        public void AddListener(int eventType, EventDelegate listener)
        {
            Delegate d;
            if (delegates.TryGetValue(eventType, out d))
                delegates[eventType] = Delegate.Combine(d, listener);
            else
                delegates[eventType] = listener;
        }

        public void AddAnyEventListener(EventDelegate listener)
        {
            if (anyEventDelegate != null)
                anyEventDelegate = Delegate.Combine(anyEventDelegate, listener);
            else
                anyEventDelegate = listener;
        }

        public void RemoveListener(int eventType, EventDelegate listener)
        {
            Delegate d;
            if (delegates.TryGetValue(eventType, out d))
            {
                var currentDel = Delegate.Remove(d, listener);

                if (currentDel == null)
                {
                    delegates.Remove(eventType);
                }
                else
                {
                    delegates[eventType] = currentDel;
                }
            }
        }

        public void RemoveAnyEventListener(EventDelegate listener)
        {
            var currentDel = Delegate.Remove(anyEventDelegate, listener);
            if (currentDel == null)
                anyEventDelegate = null;
            else
                anyEventDelegate = currentDel;
        }

        public void Raise(IEvent @event)
        {
            Delegate d;
            if (delegates.TryGetValue(@event.eventType, out d))
            {
                var callback = d as EventDelegate;
                if (callback != null)
                    callback(@event);
            }

            if (anyEventDelegate != null)
            {
                var callback = anyEventDelegate as EventDelegate;
                if (callback != null)
                    callback(@event);
            }
        }

        public void Dispose()
        {
            delegates.Clear();
            anyEventDelegate = null;
        }
    }
}