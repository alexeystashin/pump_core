﻿namespace Pump.Core
{
    public interface IEvent
    {
        int eventType { get; }
    }
}
