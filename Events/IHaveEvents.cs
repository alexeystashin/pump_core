﻿namespace Pump.Core
{
    public interface IHaveEvents
    {
        EventMessenger events { get; }
    }
}
