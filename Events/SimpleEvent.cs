﻿using System;

namespace Pump.Core
{
    public class SimpleEvent : IEvent
    {
        public int eventType { get; protected set; }

        public SimpleEvent(int eventType)
        {
            this.eventType = eventType;
        }

        public SimpleEvent(Enum eventType)
        {
            this.eventType = eventType.ToInt();
        }
    }

    public class SimpleEvent<TValue> : SimpleEvent
    {
        public TValue value { get; protected set; }

        public SimpleEvent(int eventType, TValue value)
            : base(eventType)
        {
            this.value = value;
        }

        public SimpleEvent(Enum eventType, TValue value)
            : base(eventType)
        {
            this.value = value;
        }
    }

    public class SimpleEvent<TValue,TValue2> : SimpleEvent
    {
        public TValue value { get; protected set; }
        public TValue2 value2 { get; protected set; }

        public SimpleEvent(int eventType, TValue value, TValue2 value2)
            : base(eventType)
        {
            this.value = value;
            this.value2 = value2;
        }

        public SimpleEvent(Enum eventType, TValue value, TValue2 value2)
            : base(eventType)
        {
            this.value = value;
            this.value2 = value2;
        }
    }

    public class SimpleEvent<TValue, TValue2, TValue3> : SimpleEvent
    {
        public TValue value { get; protected set; }
        public TValue2 value2 { get; protected set; }
        public TValue3 value3 { get; protected set; }

        public SimpleEvent(int eventType, TValue value, TValue2 value2, TValue3 value3)
            : base(eventType)
        {
            this.value = value;
            this.value2 = value2;
            this.value3 = value3;
        }

        public SimpleEvent(Enum eventType, TValue value, TValue2 value2, TValue3 value3)
            : base(eventType)
        {
            this.value = value;
            this.value2 = value2;
            this.value3 = value3;
        }
    }
}
