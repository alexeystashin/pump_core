﻿namespace Pump.Core
{
    public interface IClonable<TBaseClass>
    {
        TBaseClass MakeClone();
    }
}
