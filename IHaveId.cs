﻿namespace Pump.Core
{
    public interface IHaveId<TId>
    {
        TId id { get; }
    }
}
