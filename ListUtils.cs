﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pump.Core
{
    public static partial class Utils
    {
        public static void AddDistinct<T>(this List<T> list, T item)
        {
            if (!list.Contains(item))
                list.Add(item);
        }

        public static bool SafeContains<T>(this List<T> list, T item)
        {
            if (list == null)
                return false;

            return list.Contains(item);
        }

        public static bool SafeContainsAny<T>(this List<T> list, IEnumerable<T> items)
        {
            if (list == null)
                return false;

            if (items == null)
                return false;

            return items.Any(item => list.Contains(item));
        }

        public static int IndexOf<T>(this IReadOnlyList<T> self, T elementToFind)
        {
            int i = 0;
            foreach (T element in self)
            {
                if (Equals(element, elementToFind))
                    return i;
                i++;
            }
            return -1;
        }

        public static int IndexOf<T>(this IReadOnlyList<T> self, Func<T, bool> checkFunc)
        {
            int i = 0;
            foreach (T element in self)
            {
                if (checkFunc(element))
                    return i;
                i++;
            }
            return -1;
        }
    }
}
