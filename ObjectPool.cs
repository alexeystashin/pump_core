﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    // Simple Object Pool. Based on UnityEngine.UI's ObjectPool
    // https://bitbucket.org/Unity-Technologies/ui/src/f0c70f707cf09f959ad417049cb070f8e296ffe2/UnityEngine.UI/UI/Core/Utility/ObjectPool.cs
    //
    public class ObjectPool<T> : IDisposable where T : class
    {
        readonly Stack<T> objectStack = new Stack<T>();
        readonly Func<T> createFunc;
        readonly Action<T> releaseAction;
        readonly Action<T> destroyAction;

        public int poolLimit { get; private set; }
        public int countAll { get; private set; }
        public int countActive => countAll - countInactive;
        public int countInactive => objectStack.Count;

        public ObjectPool(Func<T> createFunc, Action<T> releaseAction)
        {
            this.createFunc = createFunc;
            this.releaseAction = releaseAction;
        }

        public ObjectPool(Func<T> createFunc, Action<T> releaseAction, Action<T> destroyAction, int poolLimit)
        {
            this.createFunc = createFunc;
            this.releaseAction = releaseAction;
            this.destroyAction = destroyAction;
            this.poolLimit = poolLimit;
        }

        public T Take()
        {
            T element;
            if (objectStack.Count == 0)
            {
                element = createFunc();
                countAll++;
                //DebugUtils.Log("Pool item created (+) " + countAll + "/" + countActive + "/" + countInactive);
            }
            else
            {
                element = objectStack.Pop();
                //DebugUtils.Log("Pool item reused (+) " + countAll + "/" + countActive + "/" + countInactive);
            }
            return element;
        }

        public void Release(T element)
        {
            if (objectStack.Count > 0 && ReferenceEquals(objectStack.Peek(), element))
            {
                DebugUtils.LogError("Internal error. Trying to destroy object that is already released to pool.");
            }

            if (poolLimit > 0 && countInactive >= poolLimit) // pool is full
            {
                destroyAction(element);
                countAll--;
#if false && UNITY_EDITOR
                Debug.Log("Pool item destroyed (-) " + countAll + "/" + countActive + "/" + countInactive);
#endif
            }

            releaseAction(element);
            objectStack.Push(element);
#if false && UNITY_EDITOR
            Debug.Log("Pool item released (-) " + countAll + "/" + countActive + "/" + countInactive);
#endif
        }

        public void ClearAll()
        {
            if (destroyAction == null)
            {
                objectStack.Clear();
                countAll = 0;
                return;
            }

            while (objectStack.Count > 0)
            {
                var element = objectStack.Pop();
                destroyAction(element);
                countAll--;
            }
        }

        public void Dispose()
        {
            ClearAll();

            if (countAll > 0)
            {
                DebugUtils.LogWarning("Some objects not released properly in pool " + GetType().Name
                                 + "  (" + countAll + ")");
            }
        }
    }
}