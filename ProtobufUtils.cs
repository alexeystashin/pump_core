﻿using System.IO;

namespace Pump.Core
{
    public class ProtobufUtils
    {
        private static readonly object ProtobufSyncLock = new object();

        public static byte[] SerializeObject<T>(T obj)
        {
            lock (ProtobufSyncLock)
            {
                byte[] bytes;

                using (var stream = new MemoryStream())
                {
                    ProtoBuf.Serializer.Serialize(stream, obj);
                    bytes = stream.ToArray();
                }

                return bytes;
            }
        }

        public static T DeserializeObject<T>(byte[] bytes)
        {
            lock (ProtobufSyncLock)
            {
                T obj;

                using (var stream = new MemoryStream(bytes))
                {
                    stream.SetLength(bytes.LongLength);
                    obj = ProtoBuf.Serializer.Deserialize<T>(stream);
                }

                return obj;
            }
        }
    }
}
