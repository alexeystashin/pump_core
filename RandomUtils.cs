﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Pump.Core
{
    public static class RandomUtils
    {
        public static Random SharedRandom = new Random();

        static UnicodeEncoding byteConverter = new UnicodeEncoding();
        static SHA256 sha256 = SHA256.Create();

        public static int GetSeedFromString(string str)
        {
            var hashBytes = sha256.ComputeHash(byteConverter.GetBytes(str));
            var hashCode = 0;
            for (var i = 0; i < hashBytes.Length; i++)
                // Rotate by 3 bits and XOR the new value.
                hashCode = (hashCode << 3) | (hashCode >> (29)) ^ hashBytes[i];
            return hashCode;
        }

        public static float GetInRange(float min, float max, Random random = null)
        {
            random = random ?? SharedRandom;
            return (float)(min + (max - min) * random.NextDouble());
        }

        public static int GetInRange(int minInclusive, int maxInclusive, Random random = null)
        {
            random = random ?? SharedRandom;
            return random.Next(minInclusive, maxInclusive + 1);
        }

        public static CellPoint GetInRadius(int minRadius = 0, int maxRadius = 1, Random random = null)
        {
            random = random ?? SharedRandom;
            var distance = GetInRange(minRadius, maxRadius, random);
            var angle = random.NextDouble() * Math.PI * 2;
            var x = (int)Math.Round(Math.Cos(angle) * distance);
            var z = (int)Math.Round(Math.Sin(angle) * distance);
            return new CellPoint(x, z);
        }

        public static RealPoint GetInRadius(float minRadius = 0, float maxRadius = 1, Random random = null)
        {
            random = random ?? SharedRandom;
            var distance = GetInRange(minRadius, maxRadius, random);
            var angle = random.NextDouble() * Math.PI * 2;
            var x = (float)(Math.Cos(angle) * distance);
            var z = (float)(Math.Sin(angle) * distance);
            return new RealPoint(x, z);
        }

        public static CellPoint GetInRect(CellPoint center, int sizeX, int sizeZ, Random random = null)
        {
            var halfX = (int)(sizeX * 0.5f);
            var halfZ = (int)(sizeZ * 0.5f);
            return GetInRect(center.x - halfX, center.z - halfZ, center.x + halfX, center.z + halfZ, random);
        }

        public static CellPoint GetInRect(int x0, int z0, int x1, int z1, Random random = null)
        {
            // normalize min/max values
            if (x0 > x1) { var t = x0; x0 = x1; x1 = t; }
            if (z0 > z1) { var t = z0; z0 = z1; z1 = t; }

            var x = GetInRange(x0, x1, random);
            var z = GetInRange(z0, z1, random);
            return new CellPoint(x, z);
        }

        public static T RandomEnumValue<T>(Random random = null) where T : Enum
        {
            random = random ?? SharedRandom;
            return Enum
                .GetValues(typeof(T))
                .Cast<T>()
                .OrderBy(x => random.Next())
                .FirstOrDefault();
        }

        // случайный элемент enum, кроме заданного
        public static T RandomEnumValueExceptOne<T>(T except, Random random = null)
        {
            random = random ?? SharedRandom;
            return Enum
                .GetValues(typeof(T))
                .Cast<T>()
                .Where(x => !x.Equals(except))
                .OrderBy(x => random.Next())
                .FirstOrDefault();
        }

        public static TValue RandomDictElement<TKey, TValue>(Dictionary<TKey, TValue> dict, Random random = null)
        {
            random = random ?? SharedRandom;
            if (dict == null || !dict.Any()) return default(TValue);
            return dict.ElementAt(random.Next(0, dict.Count)).Value;
        }

        public static TValue RandomListElement<TValue>(IEnumerable<TValue> list, Random random = null)
        {
            random = random ?? SharedRandom;
            return list.ElementAt(random.Next(0, list.Count()));
        }

        public static TValue RandomElement<TValue>(IEnumerable<TValue> enumerable, Random random = null)
        {
            random = random ?? SharedRandom;
            if (enumerable == null) return default(TValue);
            var elementCount = enumerable.Count();
            if (elementCount == 0) return default(TValue);
            return enumerable.ElementAt(random.Next(0, elementCount));
        }

        public static TValue RandomElement<TValue>(IEnumerable<TValue> enumerable, TValue except, Random random = null)
            where TValue : IEquatable<TValue>
        {
            random = random ?? SharedRandom;
            if (enumerable == null) return default(TValue);
            enumerable = enumerable.Where(i => !i.Equals(except));
            var elementCount = enumerable.Count();
            if (elementCount == 0) return default(TValue);
            return enumerable.ElementAt(random.Next(0, elementCount));
        }

        // выбирает один из нескольких элементов с разной вероятностью
        // пример:
        //
        // data = {"common":70,"rare":20,"very_rare":10}
        //
        // ChanceRoulette(data) вернет "common" (вероятность 70%), либо "rare" (вероятность 20%), либо "very_rare" (вероятность 10%)
        //
        // сумма шансов не обязательно должна составлять 100
        public static string ChanceRoulette(IDictionary<string, int> data, Random random = null)
        {
            return ChanceRoulette<string>(data, random);
        }

        // выбирает один из нескольких элементов с разной вероятностью
        public static TValue ChanceRoulette<TValue>(IEnumerable<KeyValuePair<TValue, int>> data, Random random = null)
        {
            random = random ?? SharedRandom;

            var currentChance = 0f;

            var totalChances = data.Sum(i => i.Value);

            var targetChance = random.Next(totalChances + 1);

            foreach (var item in data)
            {
                currentChance += item.Value;
                if (currentChance >= targetChance)
                    return item.Key;
            }

            // не должно никогда вызываться при нормальных параметрах
            return default(TValue);
        }

        // выбирает один из нескольких элементов с разной вероятностью
        public static TValue ChanceRoulette<TValue>(IList<TValue> data, IList<int> weights, Random random = null)
        {
            random = random ?? SharedRandom;

            var currentChance = 0f;

            var totalChances = weights.Sum(i => i);

            var targetChance = random.Next(totalChances + 1);

            for (var i = 0; i < weights.Count; i++)
            {
                currentChance += weights[i];
                if (currentChance >= targetChance)
                    return data[i];
            }

            // не должно никогда вызываться при нормальных параметрах
            return default(TValue);
        }

        public static void Shuffle<T>(this IList<T> list, Random rnd = null)
        {
            if (rnd == null)
                rnd = SharedRandom;

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
