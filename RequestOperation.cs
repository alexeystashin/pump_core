﻿using System;

namespace Pump.Core
{
    public class RequestOperation
    {
        public int id { get { return request != null ? request.requestId : 0; } }

        public bool success { get { return response != null && response.returnCode == 0; } }

        public PumpRequest request { get; private set; }
        public PumpResponse response { get; private set; }

        protected Action<RequestOperation> callback;

        public RequestOperation(PumpRequest request, Action<RequestOperation> callback)
        {
            this.request = request;
            this.callback = callback;
        }

        public void CompleteAsIs(PumpResponse response)
        {
            this.response = response;

            if (callback != null)
                callback.Invoke(this);

            Dispose();
        }

        public void Complete(PumpResponse response)
        {
            response.opCode = request.opCode;
            response.requestId = request.requestId;
            response.roomId = request.roomId;

            CompleteAsIs(response);
        }

        public void CompleteOk()
        {
            var response = new PumpResponse
            {
                opCode = request.opCode,
                requestId = request.requestId,
                roomId = request.roomId,
                returnCode = 0
            };
            CompleteAsIs(response);
        }

        public void Fail(short errCode = -1, string debugMessage = null)
        {
            var response = new PumpResponse
            {
                opCode = request.opCode,
                requestId = request.requestId,
                roomId = request.roomId,
                returnCode = errCode,
                debugMessage = debugMessage
            };
            CompleteAsIs(response);
        }

        public void Dispose()
        {
            request = null;
            response = null;
            callback = null;
        }
    }
}
