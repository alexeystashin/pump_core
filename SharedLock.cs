﻿using System;

namespace Pump.Core
{
    public class SharedLock
    {
        public event Action onLocked;
        public event Action onUnlocked;

        public bool isLocked => lockCounter > 0;

        int lockCounter;

        public void Lock()
        {
            var wasLocked = isLocked;

            lockCounter++;

            if (!wasLocked && isLocked)
                onLocked?.Invoke();
        }

        public void Unlock()
        {
            var wasLocked = isLocked;

            lockCounter--;

            if (wasLocked && !isLocked)
                onUnlocked?.Invoke();
        }

        public void ForceUnlock()
        {
            var wasLocked = isLocked;

            lockCounter = 0;

            if (wasLocked && !isLocked)
                onUnlocked?.Invoke();
        }

        public void Reset()
        {
            lockCounter = 0;
            onLocked = null;
            onUnlocked = null;
        }
    }
}
