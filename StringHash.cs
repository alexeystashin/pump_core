﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    [ProtoBuf.ProtoContract]
    public struct StringHash : IEquatable<StringHash>
    {
        [ProtoBuf.ProtoMember(1)]
        public int hash;

        public static Dictionary<int, string> hashCache = new Dictionary<int, string>();

        public static string TryGetString(int hash)
        {
            return hashCache.ContainsKey(hash) ? hashCache[hash] : null;
        }

        public StringHash(string str)
        {
            hash = GetDeterministicHashCode(str);

            if (!hashCache.ContainsKey(hash))
                hashCache.Add(hash, str);
            else if (hashCache[hash] != str)
                DebugUtils.LogError("[StringHash] duplicated hash: " + hashCache[hash] + " vs " + str);
        }

        public StringHash(int hash)
        {
            this.hash = hash;
        }

        public bool Equals(StringHash other)
        {
            return hash == other.hash;
        }

        public override bool Equals(Object other)
        {
            if (!(other is StringHash))
                return false;
            return hash == ((StringHash)other).hash;
        }

        public static bool operator ==(StringHash a, StringHash b)
        {
            return a.hash == b.hash;
        }

        public static bool operator !=(StringHash a, StringHash b)
        {
            return a.hash != b.hash;
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public static int GetDeterministicHashCode(string str)
        {
            if (str == null)
                return 0;

            unchecked
            {
                int hash1 = (5381 << 16) + 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1)
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }
    }
}
