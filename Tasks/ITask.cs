﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    public enum TaskState
    {
        Init,
        Running,
        Complete,
        Failed,
        Disposed
    }

    public interface ITask : IHaveEvents, IDisposable
    {
        TaskState state { get; }

        void Start();
        void Fail();
    }

    public interface ICompositeTask : ITask
    {
        IEnumerable<ITask> subTasks { get; }
 
        bool AddTask(ITask task);
    }
}
