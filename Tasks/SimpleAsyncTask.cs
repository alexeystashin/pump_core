﻿using System;
using System.Text;

namespace Pump.Core
{
    public class SimpleAsyncTask : TaskBase
    {
        Action<Action, Action> taskAction;

        string taskName;

        public SimpleAsyncTask(Action<Action, Action> taskAction, string taskName = null)
        {
            this.taskAction = taskAction;
            this.taskName = taskName;
        }

        public override void Start()
        {
            base.Start();

            taskAction.Invoke(Complete, Fail);
        }

        public override void Dispose()
        {
            base.Dispose();

            taskAction = null;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("[" + GetType().Name);
            if (taskName != null)
                sb.Append(" " + taskName);
            sb.Append("]");
            return sb.ToString();
        }
    }
}
