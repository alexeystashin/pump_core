﻿using System;
using System.Text;

namespace Pump.Core
{
    public class SimpleTask : TaskBase
    {
        Action taskAction;

        string taskName;

        public SimpleTask(Action taskAction, string taskName = null)
        {
            this.taskAction = taskAction;
            this.taskName = taskName;
        }

        public override void Start()
        {
            base.Start();

            taskAction();

            Complete();
        }

        public override void Dispose()
        {
            base.Dispose();

            taskAction = null;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("[" + GetType().Name);
            if (taskName != null)
                sb.Append(" " + taskName);
            sb.Append("]");
            return sb.ToString();
        }
    }
}
