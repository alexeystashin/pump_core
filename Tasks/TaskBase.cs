﻿using System;

namespace Pump.Core
{
    public abstract class TaskBase : ITask
    {
        public EventMessenger events { get; private set; }

        public bool isDisposed { get; private set; }

        public TaskState state { get; private set; }

        public string debugName;

        protected bool keepAlive;

        protected TaskBase()
        {
            events = new EventMessenger();
        }

        // команда запуска задачи. каждая задача опредеяет свою логику запуска
        public virtual void Start()
        {
            if (state != TaskState.Running)
            {
                state = TaskState.Running;
                events.Raise(new TaskEvent((int)TaskEventType.TaskStarted,this));
            }
        }

        // отмена, аварийное заверение при ошибке и т.п.
        public virtual void Fail()
        {
            if (state != TaskState.Complete && state != TaskState.Failed && state != TaskState.Disposed)
            {
                state = TaskState.Failed;
                events.Raise(new TaskEvent((int)TaskEventType.TaskFailed,this));
            }

            if (!keepAlive)
                Dispose();
        }

        // только сама задача может завершить себя со стейтом Complete
        protected virtual void Complete()
        {
            if(isDisposed)
                throw new Exception();

            if (state != TaskState.Complete && state != TaskState.Failed && state != TaskState.Disposed)
            {
                state = TaskState.Complete;
                events.Raise(new TaskEvent((int)TaskEventType.TaskComplete,this));
            }

            if (!keepAlive)
                Dispose();
        }

        protected virtual void ResetState()
        {
            state = TaskState.Init;
            events.Raise(new TaskEvent((int)TaskEventType.TaskInit, this));
        }

        public virtual void Dispose()
        {
            if (isDisposed) return;

            isDisposed = true;

            if (state == TaskState.Running)
            {
                state = TaskState.Failed;
                events.Raise(new TaskEvent((int)TaskEventType.TaskFailed,this));
            }

            state = TaskState.Disposed;

            events.Dispose();
            events = null;
        }

        public override string ToString()
        {
            return "[" + GetType().Name + " " + debugName+"]";
        }
    }
}
