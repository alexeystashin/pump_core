﻿namespace Pump.Core
{
    public enum TaskEventType : int
    {
        _First = 20000,

        TaskInit,
        TaskStarted,
        TaskComplete,
        TaskFailed
    }

    public class TaskEvent : IEvent
    {
        public int eventType { get; protected set; }

        public ITask task => _taskRef.Obj;

        WeakRef<ITask> _taskRef;

        public TaskEvent(int eventType, ITask task)
        {
            //debugTask = task;

            this.eventType = eventType;

            _taskRef = new WeakRef<ITask>();
            _taskRef.Set(task);
        }
    }
}