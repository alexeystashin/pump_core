﻿using System;
using System.Collections.Generic;

namespace Pump.Core
{
    public class TaskQueue : TaskBase, ICompositeTask
    {
        public enum TaskQueueMode
        {
            FailOnAnyTask,
            NeverFail
        };

        public TaskQueueMode mode { get; private set; }
        public bool autoRestart { get; private set; }

        protected readonly List<ITask> tasks = new List<ITask>();

        public IEnumerable<ITask> subTasks => tasks;

        public ITask currentTask
        {
            get
            {
                if (tasks.Count == 0)
                    return null;
                return tasks[0];
            }
        }

        public bool isEmpty { get { return tasks.Count == 0; } }

        public TaskQueue(TaskQueueMode mode = TaskQueueMode.FailOnAnyTask, bool autoRestart = false, bool keepAlive = false)
        {
            this.mode = mode;
            this.autoRestart = autoRestart;
            this.keepAlive = keepAlive || autoRestart;
        }

        public bool AddTask(ITask task)
        {
            if (state == TaskState.Disposed || (!autoRestart && (task.state == TaskState.Failed || task.state == TaskState.Complete)))
                return false;

            task.events.AddListener((int)TaskEventType.TaskComplete, OnInnerTaskComplete);
            task.events.AddListener((int)TaskEventType.TaskFailed, OnInnerTaskFailed);

            var startNext = currentTask == null && autoRestart;

            tasks.Add(task);

            if (startNext)
                Start();

            return true;
        }

        public void AddSimpleTask(Action taskAction, string taskName = null)
        {
            AddTask(new SimpleTask(taskAction, taskName));
        }

        public void AddSimpleAsyncTask(Action<Action, Action> taskAction, string taskName = null)
        {
            AddTask(new SimpleAsyncTask(taskAction, taskName));
        }

        public void RemoveTask(ITask task) // todo: сделать protected
        {
            if (!tasks.Contains(task))
                return;

            task.events.RemoveListener((int)TaskEventType.TaskComplete, OnInnerTaskComplete);
            task.events.RemoveListener((int)TaskEventType.TaskFailed, OnInnerTaskFailed);
            tasks.Remove(task);
        }

        public override void Start()
        {
            base.Start();
            TryStartNextTask();
        }

        private void TryStartNextTask()
        {
            if(tasks.Count>0)
                tasks[0].Start();
            else
                Complete();
        }

        private void OnInnerTaskComplete(IEvent @event)
        {
            var taskEvent = (TaskEvent)@event;
            var task = taskEvent.task;

            if (task == null || !tasks.Contains(task))
                throw new InvalidOperationException();

            var startNext = state == TaskState.Running && currentTask == task;

            RemoveTask(task);

            if (startNext)
                TryStartNextTask();
        }

        private void OnInnerTaskFailed(IEvent @event)
        {
            var taskEvent = (TaskEvent)@event;
            var task = taskEvent.task;

            if (task == null || !tasks.Contains(task))
                throw new Exception();

            if (mode == TaskQueueMode.FailOnAnyTask)
            {
                Fail();
                return;
            }

            var startNext = state == TaskState.Running && currentTask == task;

            RemoveTask(task);

            if (startNext)
                TryStartNextTask();
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            base.Dispose();

            foreach (var task in tasks)
            {
                task.events.RemoveListener((int)TaskEventType.TaskComplete,OnInnerTaskComplete);
                task.events.RemoveListener((int)TaskEventType.TaskFailed,OnInnerTaskFailed);
                task.Dispose();
            }
            tasks.Clear();
        }
    }
}
