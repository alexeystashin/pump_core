﻿using System;

namespace Pump.Core
{
    public struct WeakRef<T> where T : class
    {
        WeakReference _reference;

        public T Obj
        {
            get { return _reference != null && _reference.IsAlive ? (T) _reference.Target : null; }
        }

        public void Set(T reference)
        {
            _reference = new WeakReference(reference);
        }

        public void Null()
        {
            _reference = null;
        }
    }
}
